Contexte du projet
En s'appuyant sur le repo fourni, installez en local l'application.

Pour cela, appuyez-vous sur le README. N'oubliez pas d'initialiser la bonne BDD en local, avec l'utilisateur qui va bien. Les infos pour ça sont dans le fichier db.json.

Après avoir créé un compte utilisateur, regardez les différentes failles abordées.

Pour chacune, vous aurez une brève description du principe, des exemples d'écran qui pourraient poser souci ainsi que des liens pour avoir plus d'infos.

Le but est, pour chaque faille, de :

Prendre connaissance de celle-ci
La mettre en oeuvre
Ajouter un commentaire dans le code concerné pour expliquer le mode opératoire utilisé pour cela
Corriger le code en question pour y remédier.
Attention, réalisez un commit par faille abordée.

En priorité, regardez les failles suivantes (dans cet ordre) :

Injection
Cross-site Scripting
Cross-site Request Forgery
Broken Authentication (bonus : que pensez-vous du hashage choisi?)
Broken Access Control
Il sera très difficile de tout voir sur un temps aussi court, n'oubliez pas de vous appuyer sur vos camarades si besoin.
